import React, {useState, useEffect} from 'react';
import {Alert, Pressable, StyleSheet, Text, View, TextInput, Image} from 'react-native';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import ImagePicker from 'react-native-image-crop-picker';
import RNFS from "react-native-fs"

function App() {
  const [isLoading, setIsLoading] = useState(false);
  const [count, setCount] = useState(1);
  const [compania, setCompania]=useState("")
  const [nombre, setNombre] = useState("")
  const [imagen1, setImagen1]= useState(null)
  const [imagen2, setImagen2]= useState("")
  const [imagen3, setImagen3]= useState("")
  const [imagen4, setImagen4]= useState("")
  const [imagen5, setImagen5]= useState("")
  const [imagen6, setImagen6]= useState("")
  const [logo, setLogo] = useState("")
  const [logo2, setLogo2] = useState("")
  useEffect (()=>{
    const convertrirImagen = async ()=>{
      try{
        const loguito = Image.resolveAssetSource(require("./ipm.png")).uri
        const loguito2 = Image.resolveAssetSource(require("./logo.svg")).uri
        setLogo(loguito)
        setLogo2(loguito2)
      }catch(error){
        console.log(error)
      }
    }
    convertrirImagen()
  }, [])
  

  const generatePDF = async () => {
    setIsLoading(true);
    try {
        const stilos = `
        *{
            margin: 0;
            padding: 0;
        }
        body {
            margin: 0;
            height: 100vh;
        }
        .page {
            page-break-after: always;
        }
        .container {
            height:792px;
            padding: 40px;
            display: flex;
            flex-direction: column;
        }
        .header-page1{
            flex: 1;
            border-bottom: 9px solid #0C2DA0;
        }
        .logo {
            width: 160px;
            height: 160px;
        }
        .logo img{
            width: 100%;
            height: 100%;
        }
        .content-page1{  
            flex: 1;
        }
        .ingensName-page1 h1, .ingensName-page1 h2{
            text-align: right;
            margin: 20px 0;
            font-size: 30px;
            font-weight: normal;
        }
        .company-location-p1{
            font-size: 30px;
            margin: 20px 0;
            font-weight: normal;
            display: flex;
            justify-content: right;
        }
        .information-p1{
            display: flex;
            position: relative;
            bottom: -120;
        }
        .information-p1-info{
            flex: 1;
            font-size: 20px;
        }
        .bold-text{
            font-weight: bold;
        }
        .image-container{
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
            background-color:red
        }
        .imagen{
            width: calc(33.33% - 10px);
            margin-bottom: 10px;
            margin-right: 10px;
            background-color: red;
            height: 140px;
        }
        .imagen img{
            width: 100%;
            height: 100%;
            background-size: cover;
        }
        .table-title{
          display: flex;
          gap: 20px;
          height:70px;
          width:100%
        }
        .table-title-child{
          flex: 1;
          margin-bottom: 5px;
          height: 60px;
        }
        .table-title-child H2{
          font-size: 20px;
          text-align:start;
        }
        .table-title-child img{
            width: 250px;
            height: 60px;
        }
        .table-cont{
            display: flex;
            gap: 20px;
            width:100%
        }
        .table-cont-label{
          flex: 1;
          font-weight: bold;
          margin-top:10px;
        }
        .table-cont-value{
          background-color: rgb(249, 243, 248);
          padding: 5px;
          border-radius: 4px;
          flex: 1;
        }
        .img-company{
          width: 100%;
          height: 280px;
          margin: 10px 10px 0px 0px;
        }
        .img-company img{
          width: 100%;
          height: 100%;
        }
        .container-cad{
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            margin: 10px 0px;
        }
        .cad-flex{
            font-weight: bold;
            font-size: 15px;
            padding: 5px 15px;
            background-color: rgba(0, 0, 0, 0.1);
            display: flex;
        }
        .cad-flex-child{
            flex: 1;
        }
        .cad-flex-value{
            padding: 5px 10px 10px 10px ;
          text-align: justify;
        }
        .container-flex-card{
            display: flex;
            gap: 30px;

        }
        .flex-card{
            flex: 1;
            margin-top: 10px;
        }
        .flex-card-img{
            width: 100%;
            height: 200px;
        }
        .flex-card-img img{
            width: 100%;
            height: 100%;
        }
        .flex-card-title{
            margin-top: 10px;
            font-size: 18px;
        }
        .flex-card-value{
            margin-top: 10px;
            font-size: 15px;
        }
        .container-firma{
            display: flex;
            gap: 30px;
            margin-top: 15px;
        }
        .container-firma-child{
            flex: 1;
        }
        .container-firma-child-label{
            font-weight:normal;
            background-color: rgb(217, 246, 246);
            border-radius: 4px;
            padding: 5px 10px;
            font-size:13
        }
        .firma-name-container{
            display: flex;
            gap: 30px;
        }
        .firma-name-child{
            flex: 1;
            font-size: 15px;
        }
        .firma-name-label{
            flex: 1;
            font-weight: bold;
            margin-top:10px;
            font-size:12px
        }
        .firma-name-value{
            background-color: rgb(249, 243, 248);
            padding: 5px 10px;
            border-radius: 4px;
            font-size:12px
        }
        .firma-name-child img{
            width: 190px;
            height: 80px;
        }
        .box{
            display: flex;
        }
        .box-child{
            flex: 1;
        }
        `
      const htmlPortada = `
      <html>
      <head>
          <style>
          ${stilos}
              </style>
              <script>
                  function generateD (){
                      return 'kaka';
                  }
                  window.onload = function (){
                      document.getElementById('dynamicContent').innerHTML = generateD();
                  }
  
              </script>
          </head>
          <body>
              <div class="container page">
        
                <div class="header-page1">
        
                    <div class="logo">
        
                        <img src="${logo}">
                    </div>
                </div>
                <div class="content-page1">
                 <div class="ingensName-page1">
                  <h1>Aplicacion de gestion</h1>
                  <H2>IPM - Ingens facility</H2>
                  <div class="company-location-p1">
                      <span>EMPRESA</span>
                  <span>[SUCURSAL]</span>
                  </div>
                  <div class="information-p1">
                      <div class="information-p1-info"></div>
                      <div class="information-p1-info">
                          <div>
                              <span class="bold-text">NOMBRE:</span>
                              <span>juan pablo lopez castro</span>
                          </div>
                          <div>
                              <span class="bold-text">EMPRESA:</span>
                              <span>coca cola company</span>
                          </div>
                          <div>
                              <span class="bold-text">FECHA:</span>
                              <span>Mayo 31, 2024</span>
                          </div>
                      </div>
                  </div>
                 </div>
                </div>
            </div>
            </div>
        </body>
        </html>
      `;
      const htmlPage2 =`
      <html>
      <head>
          <style>
          ${stilos}
              </style>
              <script>
                  function generateD (){
                      return 'kaka';
                  }
                  window.onload = function (){
                      document.getElementById('dynamicContent').innerHTML = generateD();
                  }
  
              </script>
          </head>
          <body>
              <div class="container page">
              <div class="table-title">
              <div class="table-title-child">
                  <img src="${logo2}">
              </div>
              <div class="table-title-child"><H2>REPORTE DE VISITA LOREM IPSUM WORD TEST</H2></div>
          </div>
          <div class="table-cont">
              <div class="table-cont-label">
                  <p><span>Cliente</span></p>
              </div>
              <div class="table-cont-label">
                  <p><span>Ruc</span></p>
              </div>
          </div>
          <div class="table-cont">
              <div class="table-cont-value">
                  <p>Coca Cola</p>
              </div>
              <div class="table-cont-value">
                  <p>600345567890</p>
              </div>
          </div>
          <div class="table-cont">
              <div class="table-cont-label">
                  <p><span>Fecha</span></p>
              </div>
              <div class="table-cont-label">
                  <p><span>Hora de entrada y salida</span></p>
              </div>
          </div>
          <div class="table-cont">
              <div class="table-cont-value">
                  <p>Mayo 31, 2023</p>
              </div>
              <div class="table-cont-value">
                  <p>08:00 AM - 6:30PM</p>
              </div>
          </div>
          <div class="table-cont">
              <div class="table-cont-label">
                  <p><span>Atencion</span></p>
              </div>
              <div class="table-cont-label">
                  <p><span>Sucursal</span></p>
              </div>
          </div>
          <div class="table-cont">
              <div class="table-cont-value">
                  <p>Nombre y apellido</p>
              </div>
              <div class="table-cont-value">
                  <p>Femsa - la mega</p>
              </div>
          </div>
                <div class="img-company">
                    <img src="https://latam-green.com/wp-content/uploads/2022/02/CC-1.jpg">
                </div>
                <div class="table-cont">
                    <div class="table-cont-label">
                        <p><span>Titulo</span></p>
                    </div>
                </div>
                <div class="table-cont">
                    <div class="table-cont-value">
                        <p>COCA COLA COMPANY</p>
                    </div>
                </div>
                <div class="table-cont">
                    <div class="table-cont-label">
                        <p><span>ESTATUS DEL SISTEMA</span></p>
                    </div>
                    <div class="table-cont-label">
                        <p><span>CONTROLADORES EN AUTOMATICO</span></p>
                    </div>
                </div>
                <div class="table-cont">
                    <div class="table-cont-value">
                        <p>ON</p>
                    </div>
                    <div class="table-cont-value">
                        <p>ON</p>
                    </div>
                </div>
                <div class="container-cad">
                <div class="cad-flex">
                    <div class="cad-flex-child">
                        <span>Descripcion</span>
                    </div>
                </div>
                    <div class="cad-flex-value">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nihil repellat aut alias, explicabo accusantium officiis iusto quasi nemo temporibus quae similique ut dolor, sunt quam animi eaque voluptatibus veniam placeat. Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid exercitationem consectetur saepe, possimus excepturi voluptatum id nulla amet doloremque quo reprehenderit nam laborum illum quaerat, optio corporis quam veritatis eveniet!
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum expedita similique id animi illo ipsa quia provident cupiditate quasi suscipit magni, exercitationem laudantium ex omnis recusandae optio unde reprehenderit quod.
                    </div>
                </div>
            </div>
        </body>
        </html>
      `;
      const htmlObservation = `
      <html>
      <head>
          <style>
          ${stilos}
              </style>
              <script>
                  function generateD (){
                      return 'kaka';
                  }
                  window.onload = function (){
                      document.getElementById('dynamicContent').innerHTML = generateD();
                  }
  
              </script>
          </head>
          <body>
          <div class="container page">
          <div class="container-cad">
           <div class="cad-flex">
               <div class="cad-flex-child">
                   <span>Observacion -- 001</span>
               </div>
               <div class="cad-flex-child">
                   <span>Severidad:</span>
                   <span>Critica</span>
               </div>
               <div class="cad-flex-child">
                   <span>Estatus:</span>
                   <span>Resuelto</span>
               </div>
           </div>
           <div class="cad-flex-value">
               Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et hic, sint ad iure veniam aliquam ad iure veniam aliquam ad iure veniam aliquam  
           </div>
          </div>
          <div class="container-flex-card">
           <div class="flex-card">
               <div class="flex-card-img">
                   <img src="https://www.engine-autoparts.com/uploads/202219596/diesel-injection-pump-for-toyota-landcruiser48302706261.jpg">
               </div>
               <h3 class="flex-card-title">Titulo Imagen</h2>
               <p class="flex-card-value">Lorem ipsum dolor sit amet consectetur adipis</p>
           </div>
           <div class="flex-card">
               <div class="flex-card-img">
                   <img src="https://www.engine-autoparts.com/uploads/202219596/diesel-injection-pump-for-toyota-landcruiser48302706261.jpg">
               </div>
               <h3 class="flex-card-title">Titulo Imagen</h2>
               <p class="flex-card-value">Lorem ipsum dolor sit amet consectetur adipis</p>
           </div>
          </div>
          <div class="container-flex-card">
           <div class="flex-card">
               <div class="flex-card-img">
                   <img src="https://www.engine-autoparts.com/uploads/202219596/diesel-injection-pump-for-toyota-landcruiser48302706261.jpg">
               </div>
               <h3 class="flex-card-title">Titulo Imagen</h2>
               <p class="flex-card-value">Lorem ipsum dolor sit amet consectetur adipis</p>
           </div>
           <div class="flex-card">
               <div class="flex-card-img">
                   <img src="https://www.engine-autoparts.com/uploads/202219596/diesel-injection-pump-for-toyota-landcruiser48302706261.jpg">
               </div>
               <h3 class="flex-card-title">Titulo Imagen</h2>
               <p class="flex-card-value">Lorem ipsum dolor sit amet consectetur adipis</p>
           </div>
          </div>
          <div>
          <div class="container-firma">
              <div class="container-firma-child">
                  <h3 class="container-firma-child-label">TECNICO INGENS FACILITY</h2>
              </div>
              <div class="container-firma-child">
                  <h3 class="container-firma-child-label">CLIENTE</h2>
              </div>
          </div>
          <div class="table-cont">
              <div class="table-cont-label">NOMBRE Y APELLIDO</div>
              <div class="table-cont-label">NOMBRE Y APELLIDO</div>
          </div>
          <div class="table-cont">
              <div class="table-cont-value">Daniel Rodriguez</div>
              <div class="table-cont-value">Alvaro Garcia</div>
          </div>
          <div class="firma-name-container">
              <div class="firma-name-child firma-name-label"><span>firma:</span></div>
              <div class="firma-name-child firma-name-label"><span>firma:</span><span>Mayo 22, 2024</span></div>
          </div>
          <div class="firma-name-container">
              <div class="firma-name-child">
                  <img src="https://upload.wikimedia.org/wikipedia/commons/f/f8/Firma_Len%C3%ADn_Moreno_Garc%C3%A9s.png">
              </div>
              <div class="firma-name-child">
                  <img src="https://upload.wikimedia.org/wikipedia/commons/f/f8/Firma_Len%C3%ADn_Moreno_Garc%C3%A9s.png">
              </div>
          </div>
      </div>
       </div>
        </body>
        </html>
      `
      const options = {
        html: htmlPortada + htmlPage2 + htmlObservation,
        fileName: `invoice_${count}`,
        directory: 'Invoices',
      };
      const file = await RNHTMLtoPDF.convert(options);
      Alert.alert('Success', `PDF saved to ${file.filePath}`);
      console.log('Success', `PDF saved to ${file.filePath}`)
      
      setCount(count + 1);
      setIsLoading(false);
    } catch (error: any) {
      Alert.alert('Error', error.message);
    }
  };

  if (isLoading) {
    return <Text>Generating PDF...</Text>;
  }

  return (
    <View style={styles.container}>
    <TextInput
    placeholder='compania'
    value={compania}
    onChangeText={text => setCompania(text)}
    />
    <TextInput
    placeholder='nombre'
    value={nombre}
    onChangeText={text => setNombre(text)}
    />
    <Pressable onPress={() => {
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true
      }).then(image => {
        console.log(image);
        setImagen1(image.path)
      });
    }}>
        <Text style={styles.text}>seleccionar imagen</Text>
      </Pressable>
      <Pressable style={styles.button}  onPress={() => generatePDF()}>
        <Text style={styles.text}>Generate PDF</Text>
      </Pressable>
      <Image
      source={require("./ipm.png")}
      style={{width:200, height:200}}
      />
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#aac',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 24,
    color: '#fff',
  },
  button: {
    backgroundColor: '#6c8ee3',
    padding: 15,
    borderRadius: 10,
    margin: 20,
  },
});

export default App;
